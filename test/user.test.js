var root = require('app-root-path');
var chai = require('chai');
var chaiHttp = require('chai-http');
var User = require(`${root.path}/model`).User;
var server = 'http://localhost:8081';
var expect = chai.expect;
var testHelper = require('./test-helper');
chai.should();
chai.use(chaiHttp);

before((done) => {
    testHelper.startServer().then(()=>{
        done();
    });
});

describe('USERS', () => {
    const URL = "/api/v1/user";
    beforeEach((done) => {
       testHelper.resetDB(User).then(()=> {
           done();
       });
    });

    describe('[/GET/:userId]', () => {
        it('[#1] - It should find an agent', (done) => {
            var testUser = {
                username: 'hoangvu',
                password: 'hoangvupassword'
            };
            User.create(testUser).then(value => {
                chai.request(server).get(`${URL}/${value.id}`).send().end((err, response) => {
                    let body = response.body;
                    response.should.have.status(200);
                    body.should.have.property('type');
                    body.should.have.property('data');
                    expect(body.type).to.equal('success');
                    expect('hoangvu').to.equal(response.body.data.user.username);
                    expect('hoangvupassword').to.equal(response.body.data.user.password);
                    done();
                });
            }).catch(err => {
                throw err;
            });
        });
    });

    describe('[/POST]', () => {
        it('[#1] - It should create an agent', (done) => {
            var testUser = {
                username: 'hoangvu',
                password: 'hoangvupassword'
            };

            chai.request(server).post(`${URL}`).send(testUser).end((err, response) => {
                let body = response.body;
                response.should.have.status(200);
                body.should.have.property('type');
                body.should.have.property('data');
                expect(body.type).to.equal('success');
                done();
            });
        });
    });
});