module.exports = function (req, res, next) {
    var err = new Error('Request is not found');
    err.status = 404;
    next(err);
}

