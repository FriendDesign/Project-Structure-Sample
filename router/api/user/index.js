
var getMethod = require('./get');
var postMethod = require('./post');

module.exports = {
    insert: postMethod.insert,
    findById: getMethod.findById
}