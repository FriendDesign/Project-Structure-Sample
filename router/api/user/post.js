var rootPath = require('app-root-path');
var userModel = require(`${rootPath.path}/model`).User;
var responseUtil = require(`${rootPath.path}/util`).ResponseUtil;
var joi = require('joi');


function insert(req, res) {
        userModel.create(req.body).then(function (data) {
            responseUtil.sendSuccessResponse(res, {
                userId: data.id
            });
        }).catch(function (err) {
            responseUtil.sendErrorResponse(res, err.message, err);
        });
};

module.exports = {
    insert: insert
};