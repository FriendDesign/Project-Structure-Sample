var rootPath = require('app-root-path');
var responseUtil = require(`${rootPath.path}/util`).ResponseUtil;
var userModel = require(`${rootPath.path}/model`).User;

function findById(req, res) {
    userModel.findById({
        _id: req.params.userId
    }).then(value => {
        responseUtil.sendSuccessResponse(res, {
            user: value
        });
    }).catch(err => {
        responseUtil.sendErrorResponse(res, err.message, err);
    });
}

module.exports = {
    findById: findById
}