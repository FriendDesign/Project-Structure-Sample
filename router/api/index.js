var userAPI = require('./user');
var urlDictionary = require('../urlDictionary');

module.exports = {
    initRouterForAPI: function (router) {
        router.route(`${urlDictionary.USER}/:userId`).get(userAPI.findById);
        router.route(urlDictionary.USER).post(userAPI.insert);
    }
}