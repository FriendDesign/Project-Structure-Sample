var apiRouter = require('./api');
var routerMiddleware = require('./middleware');
var urlDictionary = require('./urlDictionary');

module.exports = function (app) {
    var router = app.Router();
    router.use(urlDictionary.ROOT, routerMiddleware.authenticate);

    apiRouter.initRouterForAPI(router);
    
    router.use(routerMiddleware.unknowRequest);
    
    return router;
}