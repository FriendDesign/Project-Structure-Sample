const ROOT = '/api/v1';
const USER = `${ROOT}/user`;


module.exports = {
    ROOT: ROOT,
    USER: USER
}