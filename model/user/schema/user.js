var Schema = require('mongoose').Schema;

var userSchema = new Schema({
    username: {
        type: String,
        required: [true, 'Name is required'],
        trim: true,
        lowercase: true,
    },
    password: {
        type: String,
        required: [true, 'Name is required'],
        trim: true,
        lowercase: true,
    },
    firstName: String,
    lastName: String
});

module.exports = userSchema;