var mongoose = require('mongoose');
var userSchema = require('./schema').User;


var userModel = mongoose.model('users', userSchema);

module.exports = userModel;