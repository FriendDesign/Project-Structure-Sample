[![build status](https://gitlab.com/FriendDesign/Project-Structure-Sample/badges/master/build.svg)](https://gitlab.com/FriendDesign/Project-Structure-Sample/commits/master)
[![coverage report](https://gitlab.com/FriendDesign/Project-Structure-Sample/badges/master/coverage.svg)](https://gitlab.com/FriendDesign/Project-Structure-Sample/commits/master)

This is a project skeleton. 
It can help us setup a new project faster. All of basic requirement for one project is already in this project.